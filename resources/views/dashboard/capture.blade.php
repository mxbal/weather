@extends('layouts.master', ['title' => 'Capture'])

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Capture</h3>
                <p class="text-subtitle text-muted">Capture Kondisi Cuaca</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="card-content">
                    <img class="card-img-top img-fluid image-cuaca" src="{{ asset('/') }}images/logo/cam.png" alt="Card image cap" style="height: 20rem">
                    <div class="card-body">
                        <h4 class="card-title"></h4>
                        <p>Waktu : <span class="time"></span></p>
                        <button class="btn btn-primary block btn-capture">Capture</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('script')
<script>
    function getCapture() {
        setInterval(function() {
            $.ajax({
                url: '{{ route("getcapture") }}',
                method: 'GET',
                type: 'GET',
                success: function(response) {
                    // console.log(response)
                    if (response.status == 'success') {
                        $(".image-cuaca").empty().attr('src', response.cuaca.nama_gambar)
                        $(".card-title").empty().append(response.cuaca.kondisi_cuaca)
                        $(".time").empty().append(response.cuaca.created_at)
                    } else {
                        // Toastify({
                        //     text: response.message,
                        //     duration: 3000,
                        //     close: true,
                        //     backgroundColor: "#dc3545",
                        // }).showToast();
                    }
                }
            })
        }, 2000);
    }

    $(".btn-capture").on('click', function() {
        $.ajax({
            url: '{{ route("reqcapture") }}',
            method: 'GET',
            type: 'GET',
            success: function(response) {
                if (response.status == 'success') {
                    Toastify({
                        text: response.message,
                        duration: 3000,
                        close: true,
                        backgroundColor: "#4fbe87",
                    }).showToast();
                } else {
                    Toastify({
                        text: response.message,
                        duration: 3000,
                        close: true,
                        backgroundColor: "#dc3545",
                    }).showToast();
                }
            }
        })
    })

    getCapture()
</script>
@endpush