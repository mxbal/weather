<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }} - Cuaca</title>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('/') }}vendors/toastify/toastify.css">
    <link rel="stylesheet" href="{{ asset('/') }}vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="{{ asset('/') }}vendors/bootstrap-icons/bootstrap-icons.css">
    <link rel="stylesheet" href="{{ asset('/') }}css/app.css">
    <link rel="shortcut icon" href="{{ asset('/') }}images/favicon.svg" type="image/x-icon">

    @stack('style')
</head>

<body>
    <div id="app">
        <div id="sidebar" class="active">
            <div class="sidebar-wrapper active">
                <div class="sidebar-header">
                    <div class="d-flex justify-content-between">
                        <div class="logo">
                            <a href="/dashboard"><img src="{{ asset('/') }}images/logo/klasifikasi.png" alt="Logo" srcset="" width="150"></a>
                        </div>
                        <div class="toggler">
                            <a href="/dashboard" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-menu">
                    <ul class="menu">
                        <li class="sidebar-title">Menu</li>

                        <li class="sidebar-item {{ request()->is('dashboard') || request()->is('/') ? 'active' : '' }}">
                            <a href="/dashboard" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('kondisi*') ? 'active' : '' }}">
                            <a href="/kondisi" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Data Kondisi Cuaca</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('klasifikasi-cuaca') ? 'active' : '' }}">
                            <a href="/klasifikasi-cuaca" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Klasifikasi Cuaca</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('capture*') ? 'active' : '' }}">
                            <a href="/capture" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Capture</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('history*') ? 'active' : '' }}">
                            <a href="/history" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>History</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('setting*') ? 'active' : '' }}">
                            <a href="/setting" class='sidebar-link'>
                                <i class="bi bi-grid-fill"></i>
                                <span>Setting</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
            </div>
        </div>
        <div id="main" class='layout-navbar'>
            <header class='mb-3'>
                <nav class="navbar navbar-expand navbar-light ">
                    <div class="container-fluid">
                        <a href="#" class="burger-btn d-block">
                            <i class="bi bi-justify fs-3"></i>
                        </a>

                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </nav>
            </header>
            <div id="main-content">

                @yield('content')

                <footer>
                    <div class="footer clearfix mb-0 text-muted">
                        <div class="float-start">
                            <p>2022 &copy; Klasifikasi Cuaca</p>
                        </div>
                        <div class="float-end">
                            <p>Crafted with <span class="text-danger"><i class="bi bi-heart-fill icon-mid"></i></span>
                                by <a href="#">Valentino Ibrahim</a></p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script src="{{ asset('/') }}js/jquery.min.js"></script>
    <script src="{{ asset('/') }}vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="{{ asset('/') }}js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('/') }}vendors/toastify/toastify.js"></script>

    <script src="{{ asset('/') }}js/main.js"></script>

    @if(session('success'))
    <script>
        Toastify({
            text: "{{ session('success') }}",
            duration: 3000,
            close: true,
            backgroundColor: "#4fbe87",
        }).showToast();
    </script>
    @endif

    @if(session('error'))
    <script>
        Toastify({
            text: "{{ session('error') }}",
            duration: 3000,
            close: true,
            backgroundColor: "#dc3545",
        }).showToast();
    </script>
    @endif

    @stack('script')
</body>

</html>