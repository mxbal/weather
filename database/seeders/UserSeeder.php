<?php

namespace Database\Seeders;

use App\Models\Capture;
use App\Models\Cuaca;
use App\Models\Location;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'name' => 'Admin Cuaca',
            'password' => bcrypt('secret')
        ]);

        $cuaca = Cuaca::create([
            'nama_gambar' => 'asd'
        ]);

        Capture::create([
            'cuaca_id' => $cuaca->id
        ]);

        Location::create([
            'location' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.6664669606826!2d106.82496411449291!3d-6.1753870622322555!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d2e764b12d%3A0x3d2ad6e1e0e9bcc8!2sNational%20Monument!5e0!3m2!1sen!2sid!4v1651714062545!5m2!1sen!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>'
        ]);
    }
}
