@extends('layouts.master', ['title' => 'History Cuaca'])

@push('style')
<link rel="stylesheet" href="{{ asset('/') }}vendors/simple-datatables/style.css">
@endpush

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Data History Cuaca</h3>
                <p class="text-subtitle text-muted">Data History Cuaca Terbaru</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="col-md-12 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-striped" id="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Kondisi Cuaca</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($histories as $history)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ Carbon\Carbon::parse($history->cuaca->created_at )->format('d/m/Y H:i:s') ?? '-' }}</td>
                                <td>{{ $history->kondisi_cuaca ?? '-' }}</td>
                                <td>{{ $history->ket }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('script')
<script src="{{ asset('/') }}vendors/simple-datatables/simple-datatables.js"></script>

<script>
    let table = document.querySelector('#table');
    let dataTable = new simpleDatatables.DataTable(table);
</script>
@endpush