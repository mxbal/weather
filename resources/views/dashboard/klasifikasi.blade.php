@extends('layouts.master', ['title' => 'Klasifikasi Cuaca'])

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Klasifikasi Cuaca</h3>
                <p class="text-subtitle text-muted">Klasifikas Kondisi Cuaca Terbaru</p>
            </div>
        </div>
    </div>
    <section class="section row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-content">
                    <img class="card-img-top img-fluid image-cuaca" src="" alt="" style="height: 20rem">
                    <div class="card-body">
                        <h4 class="card-title"></h4>
                        <p>
                            Waktu : <span class="time"></span> <br>
                            Kondisi Jendela : <span class="posisi"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-content">
                    <img class="card-img-top img-fluid img-kondisi" src="" alt="" style="height: 20rem">
                    <div class="card-body">
                        <h4 class="card-title title-kondisi"></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@push('script')
<script>
    var interval = setInterval(function() {
        $.ajax({
            url: '{{ route("getklasifikasi") }}',
            method: 'GET',
            type: 'GET',
            success: function(response) {
                // console.log(response)
                $(".image-cuaca").empty().attr('src', response.cuaca.nama_gambar)

                if (response.cuaca.kondisi_jendela == 1) {
                    var kondisi = "Terbuka";
                } else {
                    var kondisi = "Tertutup";
                }

                if (response.cuaca.kondisi_cuaca == 'Cerah') {
                    var img = Math.floor(Math.random() * 300);
                    $(".card-title").empty().append(response.cuaca.kondisi_cuaca)
                    $(".time").empty().append(response.cuaca.created_at)
                    $(".posisi").empty().append(kondisi)
                    $(".img-kondisi").empty().attr('src', "{{ asset('images/kondisi/cerah/" + img + ".jpg') }}")
                    $(".title-kondisi").empty().append("Cerah")

                    clearInterval(interval);
                }

                if (response.cuaca.kondisi_cuaca == 'Hujan') {
                    var img = Math.floor(Math.random() * 300);
                    $(".card-title").empty().append(response.cuaca.kondisi_cuaca)
                    $(".time").empty().append(response.cuaca.created_at)
                    $(".posisi").empty().append(kondisi)
                    $(".img-kondisi").empty().attr('src', "{{ asset('images/kondisi/hujan/rain" + img + ".jpg') }}")
                    $(".title-kondisi").empty().append("Hujan")

                    clearInterval(interval);
                }

                if (response.cuaca.kondisi_cuaca == 'Mendung') {
                    var img = Math.floor(Math.random() * 300);
                    $(".card-title").empty().append(response.cuaca.kondisi_cuaca)
                    $(".time").empty().append(response.cuaca.created_at)
                    $(".posisi").empty().append(kondisi)
                    $(".img-kondisi").empty().attr('src', "{{ asset('images/kondisi/mendung/cloudly" + img + ".jpg') }}")
                    $(".title-kondisi").empty().append("Mendung")

                    clearInterval(interval);
                }
            },
        })
    }, 2000);
</script>
@endpush