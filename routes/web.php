<?php

use App\Http\Controllers\CaptureController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/kondisi', [DashboardController::class, 'kondisi'])->name('kondisi');
Route::get('/klasifikasi-cuaca', [DashboardController::class, 'klasifikasi'])->name('klasifikasicuaca');
Route::get('/profile', [DashboardController::class, 'profile'])->name('profile');
Route::post('/updateprofile', [DashboardController::class, 'update'])->name('profile.update');
Route::get('/capture', [DashboardController::class, 'capture'])->name('capture.index');
Route::get('/history', [DashboardController::class, 'history'])->name('history');
Route::get('/setting', [DashboardController::class, 'setting'])->name('setting');
Route::post('/setting', [DashboardController::class, 'updateSetting'])->name('setting.update');
