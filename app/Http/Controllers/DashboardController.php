<?php

namespace App\Http\Controllers;

use App\Http\Resources\CuacaResource;
use App\Models\Cuaca;
use App\Models\History;
use App\Models\Location;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index()
    {
        $location = Location::find(1);

        return view('dashboard.index', compact('location'));
    }

    public function kondisi()
    {
        $cuaca = Cuaca::where('recognize', 1)->latest()->get();

        return view('dashboard.kondisi', compact('cuaca'));
    }

    public function klasifikasi()
    {
        return view('dashboard.klasifikasi');
    }

    public function profile()
    {
        $user = User::findOrFail(auth()->user()->id);

        return view('dashboard.profile', compact('user'));
    }

    public function capture()
    {
        return view('dashboard.capture');
    }

    public function history()
    {
        $histories = History::latest()->get();

        return view('dashboard.history', compact('histories'));
    }

    public function setting()
    {
        $location = Location::find(1);

        return view('dashboard.setting', compact('location'));
    }

    public function updateSetting(Request $request)
    {
        $attr = $request->validate([
            'location' => 'required|string',
        ]);

        try {
            DB::beginTransaction();

            $location = Location::find(1);

            $location->update($attr);

            DB::commit();

            return back()->with('success', 'Location berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }

    public function update(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|string|unique:users,username,' . auth()->user()->id,
            'name' => 'required|string',
            'image' => 'mimes:jpg,jpeg,png'
        ]);

        try {
            DB::beginTransaction();

            $user = User::findOrFail(auth()->user()->id);

            if ($request->password) {
                $attr['password'] = bcrypt($request->password);
            } else {
                $attr['password'] = $user->password;
            }

            if ($request->file('image')) {
                Storage::delete($user->image);
                $image = $request->file('image');
                $attr['image'] = $image->storeAs('images/users', Str::slug($request->username) . '.' . $image->extension());
            } else {
                $attr['image'] = $user->image;
            }

            $user->update($attr);

            DB::commit();

            return back()->with('success', 'Profile berhasil diupdate');
        } catch (\Throwable $th) {
            DB::rollBack();
            return back()->with('error', $th->getMessage());
        }
    }
}
