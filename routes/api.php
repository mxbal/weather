<?php

use App\Http\Controllers\Api\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'throttle:600,1'], function () {
    // Get Data Cuaca Terbaru
    Route::get('/getcuaca', [ApiController::class, 'getCuaca'])->name('getcuaca');
    // Send image (per 1 jam)
    Route::post('/send-image', [ApiController::class, 'sendImage'])->name('sendimage');
    // Cek foto terbaru yg belum di recognized
    Route::get('/get-new-image', [ApiController::class, 'getNewImage'])->name('getnewimage');
    // Update foto baru dengan data yg sudah di recognized
    Route::post('/update-image', [ApiController::class, 'updateImage'])->name('updateimage');
    // Untuk meminta request capture foto (Web only)
    Route::get('/req-capture', [ApiController::class, 'reqCapture'])->name('reqcapture');
    // Untuk cek apakah status capture imagenya = 1
    Route::get('/cek-capture', [ApiController::class, 'cekCapture'])->name('cekcapture');
    // Untuk kirim & update image hasil capture
    Route::post('/capture-image', [ApiController::class, 'captureImage'])->name('captureimage');
    // Untuk get data hasil capture (Web only)
    Route::get('/get-capture', [ApiController::class, 'getCapture'])->name('getcapture');
    // Get data untuk menu klasifikasi
    Route::get('/get-klasifikasi', [ApiController::class, 'getKlasifikasi'])->name('getklasifikasi');

    // Route::get('/update-klasifikasi', [ApiController::class, 'updateKlasifikasi'])->name('updateklasifikasi');
});
