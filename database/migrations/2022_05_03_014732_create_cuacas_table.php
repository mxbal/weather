<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuacasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuacas', function (Blueprint $table) {
            $table->id();
            $table->string('nama_gambar')->nullable();
            $table->integer('recognize')->default(0);
            $table->string('kondisi_cuaca')->nullable();
            $table->string('kondisi_jendela')->default(0);
            $table->integer('showed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuacas');
    }
}
