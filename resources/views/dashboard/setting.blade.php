@extends('layouts.master', ['title' => 'Setting'])

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Setting</h3>
                <p class="text-subtitle text-muted">Setting Lokasi Map Menggunakan Embed dari Google Maps.</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="col-md-12">
            <form action="{{ route('setting.update') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="location">Location</label>
                    <textarea name="location" id="location" rows="6" class="form-control">
                    {{ $location->location }}
                    </textarea>

                    @error('location')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection