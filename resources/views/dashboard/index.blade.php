@extends('layouts.master', ['title' => 'Dashboard'])

@section('content')
<div class="page-heading">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>Dashboard</h3>
                <p class="text-subtitle text-muted">Kondisi Cuaca Terbaru</p>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="col-md-12">
            {!! $location->location !!}
        </div>
    </section>
</div>
@endsection