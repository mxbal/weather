<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CuacaResource;
use App\Models\Capture;
use App\Models\Cuaca;
use App\Models\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    public function getCuaca()
    {
        try {
            DB::beginTransaction();

            $cuaca = Cuaca::where('recognize', 1)->where('kondisi_cuaca', '!=', NULL)->latest()->first();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'successfully',
                'cuaca' => CuacaResource::make($cuaca)
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => 'Tidak ada data cuaca',
            ]);
        }
    }

    // Function kirim foto
    public function sendImage()
    {
        request()->validate([
            'gambar_cuaca' => 'required'
        ]);

        if (request()->file('gambar_cuaca')) {
            try {
                DB::beginTransaction();

                $image = request()->file('gambar_cuaca');
                $imageUrl = $image->storeAs('images/cuaca', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

                $cuaca = Cuaca::create([
                    'nama_gambar' => $imageUrl
                ]);

                History::create([
                    'cuaca_id' => $cuaca->id,
                    'kondisi_cuaca' => $cuaca->kondisi_cuaca,
                    'ket' => 'Send Image'
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Kondisi cuaca berhasil disimpan',
                    'cuaca' => CuacaResource::make($cuaca)
                ]);
            } catch (\Throwable $th) {
                DB::rollBack();
                return response()->json([
                    'status' => 'failed',
                    'message' => $th->getMessage()
                ]);
            }
        }
    }

    // Cek foto baru
    public function getNewImage()
    {
        try {
            DB::beginTransaction();

            $cuaca = Cuaca::where('recognize', 0)->latest()->first();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Succes get new image',
                'id' => $cuaca->id,
                'image' => asset('storage/' . $cuaca->nama_gambar),
                'cuaca' => CuacaResource::make($cuaca)
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage()
            ]);
        }
    }

    // Update cuaca hasil recognize
    public function updateImage()
    {
        $attr = request()->validate([
            'id' => 'required|numeric',
            'kondisi_cuaca' => 'required|string',
            'kondisi_jendela' => 'required|numeric'
        ]);

        try {
            DB::beginTransaction();

            $attr['recognize'] = 1;

            $cuaca = Cuaca::findOrFail(request('id'));
            $capture = Capture::findOrFail(1);

            $cuaca->update($attr);
            // $capture->update(['cuaca_id' => $cuaca->id, 'status_capture' => 0]);

            History::create([
                'cuaca_id' => $cuaca->id,
                'kondisi_cuaca' => $cuaca->kondisi_cuaca,
                'ket' => 'Update Cuaca'
            ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Cuaca berhasil di update',
                'cuaca' => CuacaResource::make($cuaca)
            ]);
        } catch (\Throwable $th) {
            Db::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage()
            ]);
        }
    }


    public function reqCapture()
    {
        try {
            DB::beginTransaction();
            $capture = Capture::findOrFail(1);

            $capture->update(['status_capture' => 1]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Request capture foto berhasil',
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function cekCapture()
    {
        try {
            DB::beginTransaction();

            $capture = Capture::where('status_capture', 1)->first();

            DB::commit();

            if ($capture) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Capture foto tersedia',
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'Capture foto tidak tersedia'
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'failed',
                'message' => 'Capture foto tidak tersedia'
            ]);
        }
    }

    // cek capture foto
    public function captureImage()
    {
        $capture = Capture::findOrFail(1);

        if ($capture->status_capture == 1) {
            if (request()->file('gambar_cuaca')) {
                try {
                    DB::beginTransaction();

                    $image = request()->file('gambar_cuaca');
                    $imageUrl = $image->storeAs('images/cuaca', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

                    $cuaca = Cuaca::create([
                        'nama_gambar' => $imageUrl
                    ]);

                    $capture->update([
                        'cuaca_id' => $cuaca->id,
                        'status_capture' => 0,
                        'show' => 0,
                    ]);

                    History::create([
                        'cuaca_id' => $cuaca->id,
                        'kondisi_cuaca' => $cuaca->kondisi_cuaca ?? '-',
                        'ket' => 'Capture Cuaca'
                    ]);

                    DB::commit();

                    return response()->json([
                        'status' => 'success',
                        'message' => 'Kondisi cuaca berhasil diupdate',
                        'cuaca' => CuacaResource::make($cuaca),
                        'image' => asset('storage/' . $cuaca->nama_gambar)
                    ]);
                } catch (\Throwable $th) {
                    DB::rollBack();
                    return response()->json([
                        'status' => 'failed',
                        'message' => $th->getMessage()
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'No image sended'
                ]);
            }
        } else {
            return response()->json([
                'status' => 'failed',
                'message' => 'No Request'
            ]);
        }
    }

    public function getCapture()
    {
        try {
            DB::beginTransaction();
            $capture = Capture::where('show', 0)->first();
            $cuaca = Cuaca::findOrFail($capture->cuaca_id);

            $capture->update(['show' => 1]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Request capture foto berhasil',
                'cuaca' => CuacaResource::make($cuaca)
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => 'Request capture foto gagal',
            ]);
        }
    }

    public function getKlasifikasi()
    {
        try {
            DB::beginTransaction();

            $cuaca = Cuaca::latest()->first();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Succes get new image',
                'id' => $cuaca->id,
                'image' => asset('storage/' . $cuaca->nama_gambar),
                'cuaca' => CuacaResource::make($cuaca)
            ]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'message' => $th->getMessage()
            ]);
        }
    }

    // public function updateKlasifikasi()
    // {
    //     try {
    //         DB::beginTransaction();

    //         $cuaca = Cuaca::where('recognize', 1)->latest()->first();
    //         $cuaca->update(['showed' => 1]);

    //         DB::commit();

    //         return response()->json([
    //             'status' => 'success',
    //             'message' => 'Succes get new image',
    //             'id' => $cuaca->id,
    //             'image' => asset('storage/' . $cuaca->nama_gambar),
    //             'cuaca' => CuacaResource::make($cuaca)
    //         ]);
    //     } catch (\Throwable $th) {
    //         DB::rollBack();
    //         return response()->json([
    //             'status' => 'error',
    //             'message' => $th->getMessage()
    //         ]);
    //     }
    // }
}
